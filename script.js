function timeline() {
    var btNext = document.querySelector(".bt-next-frame");
    var btPrev = document.querySelector(".bt-prev-frame");
    var frames = document.querySelector(".frames");

    btNext.addEventListener("click", () => {
        frames.scrollBy(100, 0);
    });

    btPrev.addEventListener("click", () => {
        frames.scrollBy(-100, 0);
    });
}


timeline();